import './App.css';
import Home from './Components/Home';
import Login from './Components/Login';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
import ArtInt from './Components/pages/ArtInt';
import Header from './Components/header/Header';
import Footer from './Components/footer/Footer';
import BusinessCoach from './Components/pages/BusinessCoach';
import MobileApps from './Components/pages/MobileApps';

function App() {
  return (
    <Router>
    <div className="App">
    <Header/>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/home" component={ Home} />
          <Route  exact path="/login" component={ Login} />
          <Route  exact path="/artint" component={ ArtInt} />
          <Route  exact path="/businesscoach" component={ BusinessCoach} />
          <Route  exact path="/mobileapps" component={MobileApps} />
          </Switch>   
          <Footer/>

    </div>
    </Router>
  );
}

export default App;
