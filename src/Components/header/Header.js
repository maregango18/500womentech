import React from "react";
import logo from "../assets/React_logo_wordmark.png";
import { FaFacebookF ,FaInstagram } from "react-icons/fa";
import { NavLink} from "react-router-dom"
import "./header.css"

const Header= ()=>{

    return(
        <div className="header" >
            <img className="logo" src={logo} />
            <div className="menu">
                <NavLink to ="/home" style={{paddingLeft: 13, textDecoration: 'none'}}>HOME</NavLink>
                <NavLink to ="/" style={{paddingLeft: 13, textDecoration: 'none'}}>LOGIN</NavLink>
               
                <p className="logout">LOGOUT</p>
            </div>
            <div className="header_icons">
                <FaFacebookF/>
                <FaInstagram/>
            </div>
        </div>
    )
}

export default Header