import React from "react";
import { useState } from "react";
import { Redirect } from "react-router-dom";
import { NavLink } from "react-router-dom";
import Home from "../Home"
import { Alert } from "react-bootstrap";
import "./login.css";






const LoginForm =(users)=> {
  const [input, setInput] = useState('')
  const OnsubmitHandler = (e)=>{
    e.preventDefault()

    let findUser = users.users.find(user => user.email === input)
      if(findUser){
       
        window.location.href = '/home'
      }else{
       alert("This user is not registered.")
      }
    
 }
 const onChangeHandler =(e)=>{
   e.preventDefault()
  setInput(e.currentTarget.value)
}
console.log(input)
  return (
    <div className="login"  >
      <form className="loginForm" onSubmit={OnsubmitHandler} style={{ width: '20rem' }}>
        <label className="input">Username</label>
        <input className="loginInput" type="text" placeholder="Enter your email..." onChange={onChangeHandler} />
        <label className="input">Password</label>
        <input className="loginInput" type="password" placeholder="Enter your password..." />
        <input type="checkbox"></input>
        <button className="loginButton">Log In</button>
      </form>
      <img src="https://testplugin.utoweb.com/wp-content/uploads/2022/04/rawpixel-557123-unsplash.jpg" style={{ width: '26rem' }} alt="loginimg"/>
    </div>
  );
}
export default LoginForm