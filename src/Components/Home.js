import React from "react";
import Header from "./header/Header";
import Posts from "./posts/Posts";
import Footer from "./footer/Footer";
import "./home.css"

const Home = () =>{
    return(
        <>
        <img  className="home-img" src={"https://i0.wp.com/danzis.org/wp-content/uploads/2020/04/Travel-Category-Pic.png?ssl=1"} alt="travel img"/>
        <Posts/>
        </>
    )
}

export default Home;