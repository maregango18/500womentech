import React from "react";
import { Card } from "react-bootstrap";
import Image from 'react-bootstrap/Image'
import "./pages.css"



const BusinessCoach = (posts) =>{
    return(
        <div className="singleart">
            <div className="backgr"> <Image className="singleimg" src="https://testplugin.utoweb.com/wp-content/uploads/2022/04/rawpixel-799380-unsplash.jpg" style={{ width: '26rem' }}></Image></div>
        
       
        
        <Card className="card-list" style={{ width: '18rem' }} > 
                
               
                <Card.Body>  
                  <Card.Title  style={{ color: '(84,89,95)', fontSize:25}}>Business Coaching</Card.Title>
                  <Card.Text className="text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure illum ullam excepturi molestias ut quisquam, earum libero adipisci quo nobis culpa hic veniam placeat eligendi maiores assumenda porro quidem neque praesentium nemo? Quia temporibus quis ducimus inventore architecto placeat tempore, magnam, totam hic quasi error nisi similique explicabo nam nesciunt repellat repellendus corporis! Explicabo ex perspiciatis rem maxime obcaecati vel numquam odio inventore fuga. Beatae molestiae repudiandae tempore officiis neque, blanditiis perspiciatis dignissimos quibusdam odio ab deserunt quis nisi sapiente!
                  </Card.Text>
                 
                </Card.Body>
              </Card>  
         
       
 
        </div>

    )
}
export default  BusinessCoach