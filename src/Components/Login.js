import React from "react";
import { useState, useEffect } from "react";
import Footer from "./footer/Footer";
import Header from "./header/Header";
import LoginForm from "./login/LoginForm";
import "./login/login.css"


const Login = (input)=>{
    const userURL = "https://jsonplaceholder.typicode.com/users"
    const [users, setUsers] = useState([])

    useEffect(()=>{
        fetch(userURL)
        .then(res=>{
            return res.json()
        })
        .then(data=>{
            setUsers(data)
        })
    },[])
    return(
        <div className="loginpg">
           
            <LoginForm  users={users} input={input}/>
            
        </div>
    )
}

export default Login