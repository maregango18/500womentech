import React from "react";
import './footer.css'
import { FaFacebookF, FaTwitter,FaGooglePlusG, FaPinterest } from "react-icons/fa";
import {BsSuitHeartFill } from "react-icons/bs";

const Footer = () => (
  <div className="footer">
    <p>made with {<BsSuitHeartFill/>}</p>
    <p>{[ <FaFacebookF/>, <FaTwitter/>,<FaGooglePlusG/>,<FaPinterest/>]}</p>
  </div>
);

export default Footer;