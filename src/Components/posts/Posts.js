import React from "react";
import { useState, useEffect } from "react";
import PostList from "./Postlist";
import { IoRemoveOutline} from "react-icons/io5";
const URL = "https://api.npoint.io/e60f6e6cb709f8c11cff"
const Posts = () => {
    const [posts, setPosts] = useState(null)

    useEffect(()=>{
        fetch(URL)
        .then(res=>{
            return res.json()
        })
        .then(data=>{
            setPosts(data)
        })
    },[])

    return(
        <div className="post-section">
            <h1>Our Posts </h1>
            <i><IoRemoveOutline size={50}/></i>
            <p className="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eros dolor pellentesque sed luctus dapibus lobortis orci. </p>
             {posts && <PostList posts={posts} key={posts.id}/>}
            
        </div>
    )
}
export default Posts