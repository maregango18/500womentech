import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import './posts.css'
const PostList = ({ posts }) => {
   const ids = (id)=>{
        if (id===1){
          return "/ArtInt"
        }else if(id===2){
          return "/businesscoach"
        }else if(id===3){
          console.log("love")
          return "/mobileapps"
        }
        return
   }
    return (
      <div className="post-list">
        {posts.map(post => (
                <Card key={post.id} style={{ width: '18rem' }} > 
                <Card.Img variant="top" size="sm" className="card-img" src={post.images} />
                <Card.Body>  
                  <Card.Title className="title">{post.title}</Card.Title>
                  <Card.Text className="text">
                    {post.Description}
                  </Card.Text>
                  <p>{post.id}</p>
                  <Link to ={ids(post.id)} style={{paddingLeft: 13, textDecoration: 'none'}}> Learn More</Link>
                </Card.Body>
              </Card>
        
        ))}
      </div>
    )}
   
  export default PostList;

